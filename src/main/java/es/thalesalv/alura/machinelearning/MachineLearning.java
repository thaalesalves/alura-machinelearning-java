package es.thalesalv.alura.machinelearning;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

import es.thalesalv.alura.machinelearning.controller.Recommendator;
import es.thalesalv.alura.machinelearning.util.Constants;
import es.thalesalv.alura.machinelearning.util.MachineLearningUtils;

public class MachineLearning {

    private static Recommendator recommendator = new Recommendator();

    public static void main(String[] args) throws IOException, TasteException, URISyntaxException {

        Long userId = 15L;
        Integer qty = 600;
        Integer minimumRating = 7;
        DataModel model = new FileDataModel(new File(Constants.RESOURCE_PATH + "dados.csv"));
        List<RecommendedItem> recommendations = recommendator.getRecommendations(model, userId, qty);
        String errorMargin = MachineLearningUtils.formatDecimal(recommendator.getRecommendationErrorMargin(model));

        System.out.println("Products user #" + userId + " might like, with an error margin of " + errorMargin + ".");
        System.out.println("Products with a rating lower than " + minimumRating + " will be omitted.");
        for (RecommendedItem recommendation : recommendations) {
            if (recommendation.getValue() > minimumRating) {
                System.out.print("You might also like product #" + recommendation.getItemID() + " (or not, who knows?).");
                System.out.println("You'd rate it a " + MachineLearningUtils.formatDecimal(recommendation.getValue()));
            }
        }
    }
}