package es.thalesalv.alura.machinelearning.util;

import java.text.DecimalFormat;

public class MachineLearningUtils {
    public static Double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();
            
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static String formatDecimal(Double number) {
        DecimalFormat df2 = new DecimalFormat("0.00");
        return df2.format(number);
    }

    public static String formatDecimal(Float number) {
        DecimalFormat df2 = new DecimalFormat("0.00");
        return df2.format(number);
    }
}