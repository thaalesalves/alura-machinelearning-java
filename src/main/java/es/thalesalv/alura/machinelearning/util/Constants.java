package es.thalesalv.alura.machinelearning.util;

import java.io.File;

public class Constants {

    public static final String CLASSPATH = Constants.class.getResource("/").toString().replace("file:", "");
    public static final String PROJECT_PATH = new File("").getAbsolutePath();
    public static final String RESOURCE_PATH = PROJECT_PATH + "/src/main/resources/";
}