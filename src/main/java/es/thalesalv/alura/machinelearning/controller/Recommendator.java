package es.thalesalv.alura.machinelearning.controller;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;

import es.thalesalv.alura.machinelearning.recommender.ProductRecommenderBuilder;

public class Recommendator {

    public List<RecommendedItem> getRecommendations(DataModel model, Long userId, Integer qty) throws TasteException, IOException, URISyntaxException {
        RandomUtils.useTestSeed();
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);
        UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
        return recommender.recommend(userId, qty);
    }

    public Double getRecommendationErrorMargin(DataModel model) throws TasteException {
        RandomUtils.useTestSeed();
        RecommenderEvaluator evaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
        RecommenderBuilder builder = new ProductRecommenderBuilder();
        return evaluator.evaluate(builder, null, model, 0.9, 1.0);
    }

    public Recommender getRecommenderBuild(DataModel model) throws TasteException {
        return new ProductRecommenderBuilder().buildRecommender(model);
    }
}